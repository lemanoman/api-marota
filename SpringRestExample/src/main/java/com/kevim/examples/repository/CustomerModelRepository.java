package com.kevim.examples.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.kevim.model.CustomerModel;

public interface CustomerModelRepository extends CrudRepository<CustomerModel, Long> { 
 
	public List<CustomerModel> findByIdUsers(Integer idUsers);
}