package com.kevim.examples.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.kevim.model.CargoModel;

public interface CargoModelRepository extends CrudRepository<CargoModel, Integer> {
	public List<CargoModel> findByIdCustomer(Integer idCustomer);
}