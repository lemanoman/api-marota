package com.kevim.examples.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.Repository;
import com.kevim.model.UserModel;

public interface UserModelRepository extends Repository<UserModel, Long> { 
 
	@Query(value = "SELECT u FROM users u where u.email = ?1 AND u.password = ?2")
	
	public Optional<UserModel> searchByEmailAndPassword(String email, String password);
}