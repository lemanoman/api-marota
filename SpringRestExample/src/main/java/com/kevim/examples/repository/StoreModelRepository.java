package com.kevim.examples.repository;

import java.util.List;

import org.springframework.data.repository.CrudRepository;

import com.kevim.model.StoreModel;

public interface StoreModelRepository extends CrudRepository<StoreModel, Integer> { 
	
	public List<StoreModel> findByIdCustomer(Integer idCustomer);
	
	
}