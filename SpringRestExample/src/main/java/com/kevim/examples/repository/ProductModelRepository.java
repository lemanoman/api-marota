package com.kevim.examples.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.kevim.model.ProductModel;

public interface ProductModelRepository extends CrudRepository<ProductModel, Integer> { 
	public List<ProductModel> findByIdStore(Integer idStore);
	
	
	
}