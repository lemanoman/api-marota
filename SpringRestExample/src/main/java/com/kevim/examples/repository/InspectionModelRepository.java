package com.kevim.examples.repository;

import java.util.List;
import org.springframework.data.repository.CrudRepository;
import com.kevim.model.InspectionModel;

public interface InspectionModelRepository extends CrudRepository<InspectionModel, Integer> { 
	
	public List<InspectionModel> findByIdCargo(Integer idCargo);
	
	
}