package com.kevim.examples.repository;

/**
 * Created by kevim on 9/15/17.
 */

public class CargoProductModel {
    private Integer idCargoProduct;
    private Integer idCargo;
    private Integer idCustomer;
    private Integer idUsers;
    private String name;
    private String nameCN;
    private Integer totalInspected;
    private String note;
    private byte[] productImg;
    private byte[] markFrontalImg;
    private byte[] markLateralImg;
    private byte[] boxImg;
    private byte[] inspectionImg;


    public Integer getIdCargoProduct() {
        return idCargoProduct;
    }

    public void setIdCargoProduct(Integer idCargoProduct) {
        this.idCargoProduct = idCargoProduct;
    }

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public Integer getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Integer idCustomer) {
        this.idCustomer = idCustomer;
    }

    public Integer getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public byte[] getMarkFrontalImg() {
        return markFrontalImg;
    }

    public void setMarkFrontalImg(byte[] markFrontalImg) {
        this.markFrontalImg = markFrontalImg;
    }

    public byte[] getMarkLateralImg() {
        return markLateralImg;
    }

    public void setMarkLateralImg(byte[] markLateralImg) {
        this.markLateralImg = markLateralImg;
    }

    public byte[] getBoxImg() {
        return boxImg;
    }

    public void setBoxImg(byte[] boxImg) {
        this.boxImg = boxImg;
    }

    public byte[] getInspectionImg() {
        return inspectionImg;
    }

    public void setInspectionImg(byte[] inspectionImg) {
        this.inspectionImg = inspectionImg;
    }

    public Integer getTotalInspected() {
        return totalInspected;
    }

    public void setTotalInspected(Integer totalInspected) {
        this.totalInspected = totalInspected;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getNameCN() {
        return nameCN;
    }

    public void setNameCN(String nameCN) {
        this.nameCN = nameCN;
    }

    public byte[] getProductImg() {
        return productImg;
    }

    public void setProductImg(byte[] productImg) {
        this.productImg = productImg;
    }
}
