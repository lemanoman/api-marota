package com.kevim.examples.controllers;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kevim.examples.Util;
import com.kevim.examples.repository.CustomerModelRepository;
import com.kevim.examples.repository.ProductModelRepository;
import com.kevim.examples.repository.StoreModelRepository;
import com.kevim.model.CustomerModel;
import com.kevim.model.ProductModel;
import com.kevim.model.StoreModel;
import com.kevim.model.UserAndCustomerModel;


@Controller    // This means that this class is a Controller
@RequestMapping(path="/orderList") // This means URL's start with /demo (after Application path)
public class OrderListController {
	@Autowired
	private CustomerModelRepository customerRepo;
	
	@Autowired
	private StoreModelRepository storeModelRepository;
	
	@Autowired
	private ProductModelRepository productModelRepository;
		
	
	@RequestMapping(path="/list",method = RequestMethod.POST)
	public @ResponseBody List<StoreModel> findByUser (@RequestBody UserAndCustomerModel search) {
		List<StoreModel> tmp = new ArrayList<StoreModel>();
		List<StoreModel> list = storeModelRepository.findByIdCustomer(search.getIdCustomer());
		for(StoreModel model:list) {
			List<ProductModel> products = productModelRepository.findByIdStore(model.getIdStore());
			if(products!=null && !products.isEmpty()) {
				model.setTotalProducts(products.size());	
			}else {
				model.setTotalProducts(0);
			}
			
			
		}
		if(list!=null && !list.isEmpty()) {
			tmp.add(list.get(0));	
		}
		return tmp;
	}
	@RequestMapping(path="/getListProduct",method = RequestMethod.POST)
	public @ResponseBody List<ProductModel> findByStore (@RequestBody StoreModel store) {
		List<ProductModel> list = productModelRepository.findByIdStore(store.getIdStore());
		if(list!=null && !list.isEmpty()) {
			return list;	
		}
		return null;
	}
	
	@RequestMapping(path="/updateOrder",method = RequestMethod.POST)
	public @ResponseBody Integer addProduct (@RequestBody ProductModel model) {
		try {
			if(model.getIdObjectApi()!=null && model.getIdObjectApi()>0) {
				ProductModel original = productModelRepository.findOne(model.getIdObjectApi());
				if(original!=null) {
					Util.merge(original, model);
					ProductModel saved = productModelRepository.save(model);
					return saved.getIdObjectApi();	
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		return -1;
	}
	
	/**
	@RequestMapping(path="/login",method = RequestMethod.POST)
	public @ResponseBody UserModel login(@RequestBody UserModel user) {
		if(user!=null) {
			Optional<UserModel> users = userModelRepository.searchByEmailAndPassword(user.getEmail(), user.getPassword());
			if(users.isPresent()) {
				return users.get();
			}
		}
		return null;
	}
	**/
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<CustomerModel> getAllUsers() {
		// This returns a JSON or XML with the users
		return customerRepo.findAll();
	}
}