package com.kevim.examples.controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kevim.examples.repository.CustomerModelRepository;
import com.kevim.examples.repository.ProductModelRepository;
import com.kevim.examples.repository.StoreModelRepository;
import com.kevim.model.CustomerModel;
import com.kevim.model.PreOrderDTO;
import com.kevim.model.ProductModel;
import com.kevim.model.StoreModel;
import com.kevim.model.UserModel;


@Controller    // This means that this class is a Controller
@RequestMapping(path="/preOrder") // This means URL's start with /demo (after Application path)
public class PreOrderController {
	@Autowired
	private CustomerModelRepository customerRepo;
	
	@Autowired
	private StoreModelRepository storeModelRepository;
	
	@Autowired
	private ProductModelRepository productModelRepository;
	
	

	@RequestMapping(path="/addStoreAndProducts",method = RequestMethod.POST)
	public @ResponseBody String addStoreAndProducts (@RequestBody PreOrderDTO preOrder) {
		try {
			StoreModel sm = preOrder.getStore();
			if(sm!=null) {
				StoreModel returned = storeModelRepository.findOne(sm.getIdStore());
				if(returned==null) {
					sm.setIdStore(null);
					sm = storeModelRepository.save(sm);
				}else {
					sm = returned;
				}
				List<ProductModel> products = preOrder.getProducts(); 
				if(preOrder.getProducts()!=null) {
					productModelRepository.deleteAll();
					for(ProductModel productModel:products) {
						productModel.setIdStore(sm.getIdStore());
						productModelRepository.save(productModel);
					}
				}
			}
			return "true";
		}catch (Exception e) {
			e.printStackTrace();
			return "false";
		}
		
		
	}
	
	@RequestMapping(path="/findByUser",method = RequestMethod.POST)
	public @ResponseBody List<CustomerModel> findByUser (@RequestBody UserModel userModel) {
		if(userModel!=null && userModel.getIdUsers()!=null) {
			return customerRepo.findByIdUsers(userModel.getIdUsers());	
		}
		return null;
	}
	
	/**
	@RequestMapping(path="/login",method = RequestMethod.POST)
	public @ResponseBody UserModel login(@RequestBody UserModel user) {
		if(user!=null) {
			Optional<UserModel> users = userModelRepository.searchByEmailAndPassword(user.getEmail(), user.getPassword());
			if(users.isPresent()) {
				return users.get();
			}
		}
		return null;
	}
	**/
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<CustomerModel> getAllUsers() {
		// This returns a JSON or XML with the users
		return customerRepo.findAll();
	}
}