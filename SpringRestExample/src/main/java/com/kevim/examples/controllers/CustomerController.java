package com.kevim.examples.controllers;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kevim.examples.repository.CustomerModelRepository;
import com.kevim.model.CustomerModel;
import com.kevim.model.UserModel;


@Controller    // This means that this class is a Controller
@RequestMapping(path="/customer") // This means URL's start with /demo (after Application path)
public class CustomerController {
	@Autowired
	private CustomerModelRepository customerRepo;
	
	

	@RequestMapping(path="/add",method = RequestMethod.POST)
	public @ResponseBody String addNew (@RequestBody  CustomerModel model) {
		// @ResponseBody means the returned String is the response, not a view name
		// @RequestParam means it is a parameter from the GET or POST request
		
		customerRepo.save(model);
		return "Saved";
	}
	
	@RequestMapping(path="/findByUser",method = RequestMethod.POST)
	public @ResponseBody List<CustomerModel> findByUser (@RequestBody UserModel userModel) {
		if(userModel!=null && userModel.getIdUsers()!=null) {
			return customerRepo.findByIdUsers(userModel.getIdUsers());	
		}
		return null;
	}
	
	/**
	@RequestMapping(path="/login",method = RequestMethod.POST)
	public @ResponseBody UserModel login(@RequestBody UserModel user) {
		if(user!=null) {
			Optional<UserModel> users = userModelRepository.searchByEmailAndPassword(user.getEmail(), user.getPassword());
			if(users.isPresent()) {
				return users.get();
			}
		}
		return null;
	}
	**/
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<CustomerModel> getAllUsers() {
		// This returns a JSON or XML with the users
		return customerRepo.findAll();
	}
}