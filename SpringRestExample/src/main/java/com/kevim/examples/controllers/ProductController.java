package com.kevim.examples.controllers;
import java.lang.reflect.Method;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kevim.examples.repository.ProductModelRepository;
import com.kevim.examples.repository.StoreModelRepository;
import com.kevim.model.ProductModel;


@Controller    // This means that this class is a Controller
@RequestMapping(path="/product") // This means URL's start with /demo (after Application path)
public class ProductController {
	@Autowired
	private ProductModelRepository productModelRepository;
	
	@Autowired
	private StoreModelRepository storeModelRepository;
	
	

	@RequestMapping(path="/addProduct",method = RequestMethod.POST)
	public @ResponseBody Integer addProduct (@RequestBody ProductModel model) {
		try {
			if(model.getIdObjectApi()!=null && model.getIdObjectApi()>0) {
				ProductModel original = productModelRepository.findOne(model.getIdObjectApi());
				if(original!=null) {
					merge(original, model);
					ProductModel saved = productModelRepository.save(model);
					return saved.getIdObjectApi();	
				}
			}else {
				ProductModel saved = productModelRepository.save(model);
				return saved.getIdObjectApi();
			}
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		return -1;
	}
	
	@RequestMapping(path="/deleteProduct",method = RequestMethod.POST)
	public @ResponseBody String deleteProduct (@RequestBody ProductModel model) {
		productModelRepository.delete(model);
		return "true";
	}
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<ProductModel> getAll() {
		return productModelRepository.findAll();
	}
	
	
	
	public void merge(Object obj, Object update){
	    if(!obj.getClass().isAssignableFrom(update.getClass())){
	        return;
	    }

	    Method[] methods = obj.getClass().getMethods();

	    for(Method fromMethod: methods){
	        if(fromMethod.getDeclaringClass().equals(obj.getClass())
	                && fromMethod.getName().startsWith("get")){

	            String fromName = fromMethod.getName();
	            String toName = fromName.replace("get", "set");

	            try {
	                Method toMetod = obj.getClass().getMethod(toName, fromMethod.getReturnType());
	                Object value = fromMethod.invoke(update, (Object[])null);
	                if(value != null){
	                    toMetod.invoke(obj, value);
	                }
	            } catch (Exception e) {
	                e.printStackTrace();
	            } 
	        }
	    }
	}
}