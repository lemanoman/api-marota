package com.kevim.examples.controllers;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kevim.examples.Util;
import com.kevim.examples.repository.CargoModelRepository;
import com.kevim.examples.repository.InspectionModelRepository;
import com.kevim.model.CargoModel;
import com.kevim.model.CustomerModel;
import com.kevim.model.InspectionModel;
import com.kevim.model.ProductModel;


@Controller    // This means that this class is a Controller
@RequestMapping(path="/cargo") // This means URL's start with /demo (after Application path)
public class CargoController {
	
	@Autowired
	private CargoModelRepository cargoModelRepository;
	
	@Autowired
	private InspectionModelRepository inspectionModelRepository;
	
	

	@RequestMapping(path="/list",method = RequestMethod.POST)
	public @ResponseBody List<CargoModel> listCargo (@RequestBody CustomerModel model) {
		if(model!=null && model.getIdCustomer()!=null) {
			List<CargoModel> list = cargoModelRepository.findByIdCustomer(model.getIdCustomer());
			if(list!=null && !list.isEmpty()) {
				return list;
			}else{
				CargoModel cargo = new CargoModel();
				cargo.setIdCustomer(model.getIdCustomer());
				cargo.setCodeCargo("K1231321312");
				cargo.setIsFinished(0);
				cargo.setRemainInspected(90);
				cargoModelRepository.save(cargo);
				return cargoModelRepository.findByIdCustomer(model.getIdCustomer());
			}
		}
		return null;
		
	}
	
	@RequestMapping(path="/updateCargo",method = RequestMethod.POST)
	public @ResponseBody Integer updateCargo (@RequestBody CargoModel model) {
		try {
			if(model.getIdCargo()!=null && model.getIdCargo()>0) {
				CargoModel original = cargoModelRepository.findOne(model.getIdCargo());
				if(original!=null) {
					Util.merge(original, model);
					CargoModel saved = cargoModelRepository.save(model);
					return saved.getIdCargo();	
				}
			}
		}catch (Exception e) {
			e.printStackTrace();
			
		}
		return -1;
	}
	
	@RequestMapping(path="/listInspections",method = RequestMethod.POST)
	public @ResponseBody List<InspectionModel> listInspections (@RequestBody CargoModel model) {
		if(model!=null && model.getIdCustomer()!=null) {
			List<InspectionModel> list = inspectionModelRepository.findByIdCargo(model.getIdCargo());
			if(list!=null && !list.isEmpty()) {
				return list;
			}else{
				File f = new File("C:\\Users\\Kevim Such\\Pictures\\Captura_produto.png");
				InspectionModel inspectionModel = new  InspectionModel();
				inspectionModel.setIdCargo(model.getIdCargo());
				inspectionModel.setName("Produto para ninjas");
				inspectionModel.setNameCN("忍者的产品");
				inspectionModel.setNote("lalalalala");
				inspectionModel.setProductImg(fileToByteArray(f));
				inspectionModel.setTotalInspected(30);

				inspectionModelRepository.save(inspectionModel);
				return inspectionModelRepository.findByIdCargo(model.getIdCargo());
			}
		}
		return null;
		
	}
	
	public byte[] fileToByteArray(File f){
		InputStream is = null;
		ByteArrayOutputStream buffer = null;
		try {
			is = new FileInputStream(f);
			buffer = new ByteArrayOutputStream();
			
			int nRead;
			byte[] data = new byte[(int)f.length()];
			while ((nRead = is.read(data, 0, data.length)) != -1) {
			  buffer.write(data, 0, nRead);
			}
			
			buffer.flush();
			return buffer.toByteArray();
		}catch (Exception e) {
			e.printStackTrace();
		}finally {
			try {
				is.close();
				buffer.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
			
		}
		return null;
	}
	
	
	
	
}