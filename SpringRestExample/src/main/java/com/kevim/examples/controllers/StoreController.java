package com.kevim.examples.controllers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.kevim.examples.repository.StoreModelRepository;
import com.kevim.model.StoreModel;


@Controller    // This means that this class is a Controller
@RequestMapping(path="/store") // This means URL's start with /demo (after Application path)
public class StoreController {
	@Autowired
	private StoreModelRepository storeModelRepository;
	
	

	@RequestMapping(path="/addStore",method = RequestMethod.POST)
	public @ResponseBody Integer addStore (@RequestBody StoreModel model) {
		try {
			StoreModel saved = storeModelRepository.save(model);
			return saved.getIdObject();
		}catch (Exception e) {
			e.printStackTrace();
			return -1;
		}
		
	}
	
	@RequestMapping(path="/deleteStore",method = RequestMethod.POST)
	public @ResponseBody String deleteStore (@RequestBody StoreModel model) {
		storeModelRepository.delete(model);
		return "true";
	}
	
	@GetMapping(path="/all")
	public @ResponseBody Iterable<StoreModel> getAll() {
		return storeModelRepository.findAll();
	}
}