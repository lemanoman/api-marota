package com.kevim.examples;
import org.springframework.data.repository.CrudRepository;

import com.kevim.model.UserModel;


// This will be AUTO IMPLEMENTED by Spring into a Bean called userRepository
// CRUD refers Create, Read, Update, Delete

public interface UserRepository extends CrudRepository<UserModel, Long> {
	
}