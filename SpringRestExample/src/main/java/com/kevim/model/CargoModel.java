package com.kevim.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * Created by kevim on 9/15/17.
 */


@Entity(name="cargo")
public class CargoModel {
	@Id
	@GeneratedValue(strategy=GenerationType.AUTO)
	@Column
    private Integer idCargo;
	
	@Column
    private Integer idCustomer;
	
	@Column
    private String codeCargo;
	
	@Column
    private Integer isFinished;
	
	@Column
    private Integer remainInspected;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] emptyContainerImg;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] oneQuarterContainerImg;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] oneHalfContainerImg;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] threeQuartersContainerImg;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] fullContainerQuartersContainerImg;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] sealNumberContainerImg;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] closedContainerImg;

    public Integer getIdCargo() {
        return idCargo;
    }

    public void setIdCargo(Integer idCargo) {
        this.idCargo = idCargo;
    }

    public Integer getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Integer idCustomer) {
        this.idCustomer = idCustomer;
    }

    public String getCodeCargo() {
        return codeCargo;
    }

    public void setCodeCargo(String codeCargo) {
        this.codeCargo = codeCargo;
    }

    public Integer getIsFinished() {
        return isFinished;
    }

    public void setIsFinished(Integer isFinished) {
        this.isFinished = isFinished;
    }

    public Integer getRemainInspected() {
        return remainInspected;
    }

    public void setRemainInspected(Integer remainInspected) {
        this.remainInspected = remainInspected;
    }

    public byte[] getEmptyContainerImg() {
        return emptyContainerImg;
    }

    public void setEmptyContainerImg(byte[] emptyContainerImg) {
        this.emptyContainerImg = emptyContainerImg;
    }

    public byte[] getOneQuarterContainerImg() {
        return oneQuarterContainerImg;
    }

    public void setOneQuarterContainerImg(byte[] oneQuarterContainerImg) {
        this.oneQuarterContainerImg = oneQuarterContainerImg;
    }

    public byte[] getThreeQuartersContainerImg() {
        return threeQuartersContainerImg;
    }

    public void setThreeQuartersContainerImg(byte[] threeQuartersContainerImg) {
        this.threeQuartersContainerImg = threeQuartersContainerImg;
    }

    public byte[] getFullContainerQuartersContainerImg() {
        return fullContainerQuartersContainerImg;
    }

    public void setFullContainerQuartersContainerImg(byte[] fullContainerQuartersContainerImg) {
        this.fullContainerQuartersContainerImg = fullContainerQuartersContainerImg;
    }

    public byte[] getSealNumberContainerImg() {
        return sealNumberContainerImg;
    }

    public void setSealNumberContainerImg(byte[] sealNumerContainerImg) {
        this.sealNumberContainerImg = sealNumerContainerImg;
    }

    public byte[] getClosedContainerImg() {
        return closedContainerImg;
    }

    public void setClosedContainerImg(byte[] closedContainerImg) {
        this.closedContainerImg = closedContainerImg;
    }

    public byte[] getOneHalfContainerImg() {
        return oneHalfContainerImg;
    }

    public void setOneHalfContainerImg(byte[] oneHalfContainerImg) {
        this.oneHalfContainerImg = oneHalfContainerImg;
    }
}
