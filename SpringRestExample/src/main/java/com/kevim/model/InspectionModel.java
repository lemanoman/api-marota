package com.kevim.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * Created by kevim on 9/15/17.
 */

@Entity(name="inspection")
public class InspectionModel {
	
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
    private Integer idInspection;
	
	@Column
    private Integer idCargo;
	
	@Column
    private String name;
	
	@Column
    private String nameCN;
	
	@Column
    private Integer totalInspected;
	
	@Column
    private String note;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] productImg;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] markFrontalImg;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] markLateralImg;
    
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] boxImg;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] inspectionImg;
	
	
	
	public Integer getIdCargo() {
		return idCargo;
	}
	public void setIdCargo(Integer idCargo) {
		this.idCargo = idCargo;
	}
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	public String getNameCN() {
		return nameCN;
	}
	public void setNameCN(String nameCN) {
		this.nameCN = nameCN;
	}
	public Integer getTotalInspected() {
		return totalInspected;
	}
	public void setTotalInspected(Integer totalInspected) {
		this.totalInspected = totalInspected;
	}
	public String getNote() {
		return note;
	}
	public void setNote(String note) {
		this.note = note;
	}
	public byte[] getProductImg() {
		return productImg;
	}
	public void setProductImg(byte[] productImg) {
		this.productImg = productImg;
	}
	public byte[] getMarkFrontalImg() {
		return markFrontalImg;
	}
	public void setMarkFrontalImg(byte[] markFrontalImg) {
		this.markFrontalImg = markFrontalImg;
	}
	public byte[] getMarkLateralImg() {
		return markLateralImg;
	}
	public void setMarkLateralImg(byte[] markLateralImg) {
		this.markLateralImg = markLateralImg;
	}
	public byte[] getBoxImg() {
		return boxImg;
	}
	public void setBoxImg(byte[] boxImg) {
		this.boxImg = boxImg;
	}
	public byte[] getInspectionImg() {
		return inspectionImg;
	}
	public void setInspectionImg(byte[] inspectionImg) {
		this.inspectionImg = inspectionImg;
	}

    
}
