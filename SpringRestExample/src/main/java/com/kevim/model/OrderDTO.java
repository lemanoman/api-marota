package com.kevim.model;

import java.util.List;

public class OrderDTO {
	
	private StoreModel store;
	private List<ProductModel> products;
	public StoreModel getStore() {
		return store;
	}
	public void setStore(StoreModel store) {
		this.store = store;
	}
	public List<ProductModel> getProducts() {
		return products;
	}
	public void setProducts(List<ProductModel> products) {
		this.products = products;
	}
	
	

}
