package com.kevim.model;

/**
 * Created by kevim on 9/15/17.
 */

public class ShippingModel {
    private Integer idProduct;
    private Integer idStore;
    private Integer idCustomer;
    private Integer idUsers;
    private byte[] markFrontalImg;
    private byte[] markLateralImg;
    private byte[] boxImg;
    private byte[] inspectionImg;
    private byte[] tagImg;
    private Integer totalInspected;
    private Integer isBarcodeOk;
    private String note;
    private String ref;

    public Integer getIdProduct() {
        return idProduct;
    }

    public void setIdProduct(Integer idProduct) {
        this.idProduct = idProduct;
    }

    public Integer getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Integer idCustomer) {
        this.idCustomer = idCustomer;
    }

    public Integer getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public byte[] getMarkFrontalImg() {
        return markFrontalImg;
    }

    public void setMarkFrontalImg(byte[] markFrontalImg) {
        this.markFrontalImg = markFrontalImg;
    }

    public byte[] getMarkLateralImg() {
        return markLateralImg;
    }

    public void setMarkLateralImg(byte[] markLateralImg) {
        this.markLateralImg = markLateralImg;
    }

    public byte[] getBoxImg() {
        return boxImg;
    }

    public void setBoxImg(byte[] boxImg) {
        this.boxImg = boxImg;
    }

    public byte[] getInspectionImg() {
        return inspectionImg;
    }

    public void setInspectionImg(byte[] inspectionImg) {
        this.inspectionImg = inspectionImg;
    }

    public byte[] getTagImg() {
        return tagImg;
    }

    public void setTagImg(byte[] tagImg) {
        this.tagImg = tagImg;
    }

    public Integer getTotalInspected() {
        return totalInspected;
    }

    public void setTotalInspected(Integer totalInspected) {
        this.totalInspected = totalInspected;
    }

    public Integer getIsBarcodeOk() {
        return isBarcodeOk;
    }

    public void setIsBarcodeOk(Integer isBarcodeOk) {
        this.isBarcodeOk = isBarcodeOk;
    }

    public String getNote() {
        return note;
    }

    public void setNote(String note) {
        this.note = note;
    }

    public String getRef() {
        return ref;
    }

    public void setRef(String ref) {
        this.ref = ref;
    }

    public Integer getIdStore() {
        return idStore;
    }

    public void setIdStore(Integer idStore) {
        this.idStore = idStore;
    }
}
