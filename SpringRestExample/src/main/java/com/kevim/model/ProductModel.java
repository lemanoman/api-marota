package com.kevim.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * Created by kevim on 9/15/17.
 */

@Entity(name="product")
public class ProductModel {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer idObjectApi;
	
	@Column
	private Integer idProduct;
	
	@Column
    private Integer idStore;
	
	@Column
    private Integer idUsers;
	
	@Column
    private Integer idCustomer;
	
	@Column
    private Integer isInspected;
    
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] productImg;
    
    @Column
    private String createdDate;
    
    @Column
    private String name;
    
    @Column
    private String nameCN;
    
    @Column
    private String description;
    
    @Column
    private String descriptionCN;
    
    @Column
    private String cNo;
    
    @Column
    private Integer uCt;
    
    @Column
    private Double uPrice;
    
    @Column
    private String refShop;
    
    @Column
    private String unit;
    
    @Column
    private Integer l;
    
    @Column
    private Integer w;
    
    @Column
    private Integer h;
    
    @Column
    private Double gW;
    
    @Column
    private Double nW;
    
    @Column
    private String obs;
    
    @Column
    private Integer qty;
    
    @Column
    private Integer type;

	public Integer getIdProduct() {
		return idProduct;
	}

	public void setIdProduct(Integer idProduct) {
		this.idProduct = idProduct;
	}

	public Integer getIdStore() {
		return idStore;
	}

	public void setIdStore(Integer idStore) {
		this.idStore = idStore;
	}

	public Integer getIdUsers() {
		return idUsers;
	}

	public void setIdUsers(Integer idUsers) {
		this.idUsers = idUsers;
	}

	public Integer getIdCustomer() {
		return idCustomer;
	}

	public void setIdCustomer(Integer idCustomer) {
		this.idCustomer = idCustomer;
	}

	public Integer getIsInspected() {
		return isInspected;
	}

	public void setIsInspected(Integer isInspected) {
		this.isInspected = isInspected;
	}

	public byte[] getProductImg() {
		return productImg;
	}

	public void setProductImg(byte[] productImg) {
		this.productImg = productImg;
	}

	public String getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(String createdDate) {
		this.createdDate = createdDate;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getDescriptionCN() {
		return descriptionCN;
	}

	public void setDescriptionCN(String descriptionCN) {
		this.descriptionCN = descriptionCN;
	}

	public String getcNo() {
		return cNo;
	}

	public void setcNo(String cNo) {
		this.cNo = cNo;
	}

	public Integer getuCt() {
		return uCt;
	}

	public void setuCt(Integer uCt) {
		this.uCt = uCt;
	}

	public Double getuPrice() {
		return uPrice;
	}

	public void setuPrice(Double uPrice) {
		this.uPrice = uPrice;
	}

	public String getRefShop() {
		return refShop;
	}

	public void setRefShop(String refShop) {
		this.refShop = refShop;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public Integer getL() {
		return l;
	}

	public void setL(Integer l) {
		this.l = l;
	}

	public Integer getW() {
		return w;
	}

	public void setW(Integer w) {
		this.w = w;
	}

	public Integer getH() {
		return h;
	}

	public void setH(Integer h) {
		this.h = h;
	}

	public Double getgW() {
		return gW;
	}

	public void setgW(Double gW) {
		this.gW = gW;
	}

	public Double getnW() {
		return nW;
	}

	public void setnW(Double nW) {
		this.nW = nW;
	}

	public String getObs() {
		return obs;
	}

	public void setObs(String obs) {
		this.obs = obs;
	}

	

	public Integer getQty() {
		return qty;
	}

	public void setQty(Integer qty) {
		this.qty = qty;
	}

	public Integer getIdObjectApi() {
		return idObjectApi;
	}

	public void setIdObjectApi(Integer idObjectApi) {
		this.idObjectApi = idObjectApi;
	}

	public Integer getType() {
		return type;
	}

	public void setType(Integer type) {
		this.type = type;
	}

	public String getNameCN() {
		return nameCN;
	}

	public void setNameCN(String nameCN) {
		this.nameCN = nameCN;
	}

    
	

    
}
