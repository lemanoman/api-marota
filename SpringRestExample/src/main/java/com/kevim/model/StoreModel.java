package com.kevim.model;

import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;

/**
 * Created by kevim on 9/15/17.
 */

@Entity(name="store")
public class StoreModel {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	@Column
	private Integer idObject;
	
	@Column
    private Integer idStore;
	
	@Column
	private Integer idUsers;
	
	@Column
    private Integer idCustomer;
    
	@Column
    private Integer totalProducts;
	
	@Column
    private String createdDate;
	
	@Column
    private Integer idStatus;
	
	@Lob 
	@Basic(fetch = FetchType.LAZY)
	@Column(length=100000)
    private byte[] storeImg;


    public Integer getIdCustomer() {
        return idCustomer;
    }

    public void setIdCustomer(Integer idCustomer) {
        this.idCustomer = idCustomer;
    }

    public Integer getIdUsers() {
        return idUsers;
    }

    public void setIdUsers(Integer idUsers) {
        this.idUsers = idUsers;
    }

    public Integer getIdStore() {
        return idStore;
    }

    public void setIdStore(Integer idStore) {
        this.idStore = idStore;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public byte[] getStoreImg() {
        return storeImg;
    }

    public void setStoreImg(byte[] storeImgPath) {
        this.storeImg = storeImgPath;
    }

    public Integer getTotalProducts() {
        return totalProducts;
    }

    public void setTotalProducts(Integer totalProducts) {
        this.totalProducts = totalProducts;
    }

	public Integer getIdObject() {
		return idObject;
	}

	public void setIdObject(Integer idObject) {
		this.idObject = idObject;
	}

	public Integer getIdStatus() {
		return idStatus;
	}

	public void setIdStatus(Integer idStatus) {
		this.idStatus = idStatus;
	}
    
    
}
